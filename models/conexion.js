import mysql from 'mysql2';

let connection;

function createConnection(){
    connection = mysql.createConnection({
    host:"127.0.0.1",
    user:"root",
    password:"upsin123",
    database:"sistemas"
    });

    connection.connect(function (error){
        if(error){
            console.error('Conexión Fallida');
            return;
        } else{
            console.log('Conexión Exitosa')
        }
    });

    return connection;
}

export function getConnection() {
    if (!connection) {
        return createConnection();
    }
    return connection;
}